package com.pastreams.services.streamsdb2;

import java.io.Serializable;
import javax.persistence.*;
import javax.persistence.Id;

/**
 * The persistent class for the waterpropertyvalue database table.
 * 
 */
@Entity
@Table(name="waterpropertyvalue")
//@NamedQuery(name="Waterpropertyvalue.findAll", query="SELECT w FROM Waterpropertyvalue w")
public class Waterpropertyvalue implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Column(length=240)
	private String propertyvalue;

	//bi-directional many-to-one association to Water
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="waterid", referencedColumnName="waterid", nullable=false)
	private Water water;

	//bi-directional many-to-one association to Waterpropertydescription
	@Id
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="waterpropertyvalueid", referencedColumnName="waterpropertydescriptionid", nullable=false)
	private Waterpropertydescription waterpropertydescription;

	public Waterpropertyvalue() {
	}
	
	public String getPropertyvalue() {
		return this.propertyvalue;
	}

	public void setPropertyvalue(String propertyvalue) {
		this.propertyvalue = propertyvalue;
	}

	public Water getWater() {
		return this.water;
	}

	public void setWater(Water water) {
		this.water = water;
	}

	public Waterpropertydescription getWaterpropertydescription() {
		return this.waterpropertydescription;
	}

	public void setWaterpropertydescription(Waterpropertydescription waterpropertydescription) {
		this.waterpropertydescription = waterpropertydescription;
	}

}