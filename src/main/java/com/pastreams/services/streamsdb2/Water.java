package com.pastreams.services.streamsdb2;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

/**
 * The persistent class for the water database table.
 * 
 */
@Entity
@Table(name="water")
@NamedQuery(name="Water.findAll", query="SELECT w FROM Water w")
public class Water implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(insertable=false, updatable=false)
	private int waterid;

	@Column(precision=10, scale=11)
	private BigDecimal latitude;

	@Column(precision=10, scale=11)
	private BigDecimal longitude;

	@Column(length=120)
	private String name;

	@Column(precision=10, scale=3)
	private BigDecimal zoomlevel;

	//bi-directional many-to-one association to Watertype
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="watertypeid", referencedColumnName="watertypeid", nullable=false)
	private Watertype watertype;

	//bi-directional many-to-one association to Waterpropertyvalue
	@OneToMany(mappedBy="water")
	private List<Waterpropertyvalue> waterpropertyvalues;

	//bi-directional many-to-one association to Coordinate
	@OneToMany(mappedBy="water")
	//@JoinColumn(name="coordinateid", referencedColumnName="waterid")
	private List<Coordinate> coordinates;

	public Water() {
	}

	public BigDecimal getLatitude() {
		return this.latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return this.longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	private String getName() {
		return this.name;
	}

	private void setName(String name) {
		this.name = name;
	}

	public BigDecimal getZoomlevel() {
		return this.zoomlevel;
	}

	public void setZoomlevel(BigDecimal zoomlevel) {
		this.zoomlevel = zoomlevel;
	}

	public Watertype getWatertype() {
		return this.watertype;
	}

	public void setWatertype(Watertype watertype) {
		this.watertype = watertype;
	}

	public List<Waterpropertyvalue> getWaterpropertyvalues() {
		return this.waterpropertyvalues;
	}

	public void setWaterpropertyvalues(List<Waterpropertyvalue> waterpropertyvalues) {
		this.waterpropertyvalues = waterpropertyvalues;
	}

	public Waterpropertyvalue addWaterpropertyvalue(Waterpropertyvalue waterpropertyvalue) {
		getWaterpropertyvalues().add(waterpropertyvalue);
		waterpropertyvalue.setWater(this);

		return waterpropertyvalue;
	}

	public Waterpropertyvalue removeWaterpropertyvalue(Waterpropertyvalue waterpropertyvalue) {
		getWaterpropertyvalues().remove(waterpropertyvalue);
		waterpropertyvalue.setWater(null);

		return waterpropertyvalue;
	}

	public List<Coordinate> getCoordinates() {
		return this.coordinates;
	}

	public void setCoordinates(List<Coordinate> coordinates) {
		this.coordinates = coordinates;
	}

	public Coordinate addCoordinate(Coordinate coordinate) {
		getCoordinates().add(coordinate);
		coordinate.setWater(this);

		return coordinate;
	}

	public Coordinate removeCoordinate(Coordinate coordinate) {
		getCoordinates().remove(coordinate);
		coordinate.setWater(null);

		return coordinate;
	}

}