package com.pastreams.services.streamsdb2;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the waterpropertydescription database table.
 * 
 */
@Entity
@Table(name="waterpropertydescription")
@NamedQuery(name="Waterpropertydescription.findAll", query="SELECT w FROM Waterpropertydescription w")
public class Waterpropertydescription implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column()
	private int waterpropertydescriptionid;
	
	@Column(length=320)
	private String propertydescription;

	@Column(length=120)
	private String propertyname;

	//bi-directional many-to-one association to Watertype
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="watertypeid", referencedColumnName="watertypeid", nullable=false)
	private Watertype watertype;

	//bi-directional many-to-one association to Waterpropertyvalue
	@OneToMany(mappedBy="waterpropertydescription")
	private List<Waterpropertyvalue> waterpropertyvalues;

	public Waterpropertydescription() {
	}

	public String getPropertydescription() {
		return this.propertydescription;
	}

	private void setPropertydescription(String propertydescription) {
		this.propertydescription = propertydescription;
	}

	public String getPropertyname() {
		return this.propertyname;
	}

	private void setPropertyname(String propertyname) {
		this.propertyname = propertyname;
	}

	public Watertype getWatertype() {
		return this.watertype;
	}

	public void setWatertype(Watertype watertype) {
		this.watertype = watertype;
	}

	public List<Waterpropertyvalue> getWaterpropertyvalues() {
		return this.waterpropertyvalues;
	}

	public void setWaterpropertyvalues(List<Waterpropertyvalue> waterpropertyvalues) {
		this.waterpropertyvalues = waterpropertyvalues;
	}

	public Waterpropertyvalue addWaterpropertyvalue(Waterpropertyvalue waterpropertyvalue) {
		getWaterpropertyvalues().add(waterpropertyvalue);
		waterpropertyvalue.setWaterpropertydescription(this);

		return waterpropertyvalue;
	}

	public Waterpropertyvalue removeWaterpropertyvalue(Waterpropertyvalue waterpropertyvalue) {
		getWaterpropertyvalues().remove(waterpropertyvalue);
		waterpropertyvalue.setWaterpropertydescription(null);

		return waterpropertyvalue;
	}

}