package com.pastreams.services.streamsdb2;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the watertype database table.
 * 
 */
@Entity
@Table(name="watertype")
@NamedQuery(name="Watertype.findAll", query="SELECT w FROM Watertype w")
public class Watertype implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(insertable=false, updatable=false)
	private int watertypeid;

	@Column(length=120)
	private String name;

	//bi-directional many-to-one association to Water
	@OneToMany(mappedBy="watertype")
	private List<Water> waters;

	//bi-directional many-to-one association to Waterpropertydescription
	@OneToMany(mappedBy="watertype")
	private List<Waterpropertydescription> waterpropertydescriptions;

	public Watertype() {
	}

	private String getName() {
		return this.name;
	}

	private void setName(String name) {
		this.name = name;
	}

	public List<Water> getWaters() {
		return this.waters;
	}

	public void setWaters(List<Water> waters) {
		this.waters = waters;
	}

	public Water addWater(Water water) {
		getWaters().add(water);
		water.setWatertype(this);

		return water;
	}

	public Water removeWater(Water water) {
		getWaters().remove(water);
		water.setWatertype(null);

		return water;
	}

	public List<Waterpropertydescription> getWaterpropertydescriptions() {
		return this.waterpropertydescriptions;
	}

	public void setWaterpropertydescriptions(List<Waterpropertydescription> waterpropertydescriptions) {
		this.waterpropertydescriptions = waterpropertydescriptions;
	}

	public Waterpropertydescription addWaterpropertydescription(Waterpropertydescription waterpropertydescription) {
		getWaterpropertydescriptions().add(waterpropertydescription);
		waterpropertydescription.setWatertype(this);

		return waterpropertydescription;
	}

	public Waterpropertydescription removeWaterpropertydescription(Waterpropertydescription waterpropertydescription) {
		getWaterpropertydescriptions().remove(waterpropertydescription);
		waterpropertydescription.setWatertype(null);

		return waterpropertydescription;
	}

}