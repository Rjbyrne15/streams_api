package com.pastreams.services.streamsdb2;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the coordinate database table.
 * 
 */
@Entity
@Table(name="coordinate")
@NamedQuery(name="Coordinate.findAll", query="SELECT c FROM Coordinate c")
public class Coordinate implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(insertable=false, updatable=false)
	private int coorid;

	@Column( precision=10, scale=9)
	private BigDecimal lat;

	@Column(precision=10, scale=9)
	private BigDecimal lon;

	//bi-directional many-to-one association to Water
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="coordinateid", referencedColumnName="waterid")
	private Water water;

	public Coordinate() {
	}

	public BigDecimal getLat() {
		return this.lat;
	}

	private void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLon() {
		return this.lon;
	}

	private void setLon(BigDecimal lon) {
		this.lon = lon;
	}

	public Water getWater() {
		return this.water;
	}

	public void setWater(Water water) {
		this.water = water;
	}

}