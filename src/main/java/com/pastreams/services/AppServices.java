package com.pastreams.services;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class AppServices 
{
	public static void main(String[] args) {
		SpringApplication.run(AppServices.class, args);
	}
}