package com.pastreams.services.streamservice;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;

import org.springframework.stereotype.Service;

import com.pastreams.services.streamsdb2.Coordinate;
import com.pastreams.services.streamsdb2.Water;
import com.pastreams.services.streamsdb2.Waterpropertyvalue;

@Service
public class NewStreamService {
	
	public Iterable<Water> getWaterByTypeInBox(BigDecimal lat, BigDecimal lon){
		return null;
	}
	
	//json obj featurecollection (type : "name") (crs:obj) "crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:EPSG::4269" } },
	//json array features
	//json obj feature (type : feature) (properties:obj) (geometry:obj)
	//json obj properties (key, value)
	//json obj geometry (type:linestring) (coordinates:array)
	//coordinates json array of json array pairs
	
	public String buildGeoJsonFromIterableWater(Iterable<Water> waters) {
		
		Iterator<Water> itr = waters.iterator();
		JsonObjectBuilder featuresCollection = Json.createObjectBuilder();
		featuresCollection.add("type", "FeatureCollection");
		
		JsonArrayBuilder features = Json.createArrayBuilder();
		
		while(itr.hasNext()) {
			Water water = itr.next();
			
			JsonObjectBuilder feature = Json.createObjectBuilder();
			feature.add("type", "Feature");
			JsonObjectBuilder properties = Json.createObjectBuilder();
			for(Waterpropertyvalue wpv : water.getWaterpropertyvalues()) {
				properties.add(wpv.getWaterpropertydescription().getPropertyname(), wpv.getPropertyvalue());
			}
			
			JsonObjectBuilder geometry = Json.createObjectBuilder();
			geometry.add("type", "LineString");
			JsonArrayBuilder coordinates = Json.createArrayBuilder();
			List<Coordinate> coorList = water.getCoordinates();   
			for(int i =0;i <coorList.size();i++) {
				JsonArrayBuilder coordinate = Json.createArrayBuilder();
				coordinate.add(coorList.get(i).getLon());
				coordinate.add(coorList.get(i).getLat());
				coordinates.add(coordinate.build());
			}
			geometry.add("coordinates", coordinates.build());
			feature.add("properties", properties.build());
			feature.add("geometry", geometry.build());
			features.add(feature.build());
			
		}
		featuresCollection.add("features",features.build());
		
		return featuresCollection.build().toString();
	}
	

}
