package com.pastreams.services.controller;

import java.math.BigDecimal;
import java.util.List;

import javax.json.JsonObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pastreams.services.repository.WaterRepo2;


import com.pastreams.services.streamservice.NewStreamService;


//0	wilderness
//1	classa
//2	naturalrepro
//3	specialregstream
//4	specialreglake
//5	stockedwaterbodies
//6	stockedstreams


@Controller
@CrossOrigin(origins = "*")
public class AppController {
	private static final Logger LOGGER = LoggerFactory.getLogger(AppController.class);
	public static final String ASTREAMS="astreams";
    public static final String WILDSTREAMS="wildstreams";
    public static final String NATREPROSTREAMS="natreprostreams";
    public static final String STOCKEDSTREAMS="stockedstreams";
    public static final String STOCKEDWATERS="stockedwaters";
    public static final String SPECIALREGLAKES="specialreglakes";
    public static final String SPECIALREGSTREAMS="specialregstreams";
	//StreamService streamService;
	//@Autowired
	//StreamsRepository sr;
	@Autowired
	WaterRepo2 waterRepo;
	
	
	@Autowired
	NewStreamService streamService;// = new StreamService();
	
	@RequestMapping(value = "/app/get/"+ASTREAMS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Void> getAStreams() {
		LOGGER.info("Get A Streams");
		return ResponseEntity.status(HttpStatus.OK).build();
	}
	
	//Hell Creek
	@RequestMapping(value = "/app/get/"+ASTREAMS+"/name/{name}", method = RequestMethod.GET)
	public @ResponseBody String getAStreams(@PathVariable String name) {
		LOGGER.info("Get A Streams");
		return streamService.buildGeoJsonFromIterableWater(waterRepo.findStreamAndCoorsByName(name));

	}
	
	//findStreamsByLocAndType(BigDecimal lat1, BigDecimal lon1,BigDecimal lat2, BigDecimal lon2, int type);
	@RequestMapping(value = "/app/get/"+ASTREAMS+"/zoom/{zoom}/loc/{lat1}/{lon1}/{lat2}/{lon2}", method = RequestMethod.GET)
	public @ResponseBody String getAStreams(@PathVariable BigDecimal lat1,@PathVariable BigDecimal lon1,@PathVariable BigDecimal lat2
			,@PathVariable BigDecimal lon2,@PathVariable Double zoom) {
		LOGGER.info("Get Streams by loc");
		long initTime = System.currentTimeMillis();
		 String returnVal = streamService.buildGeoJsonFromIterableWater(waterRepo.findStreamsByLocAndType(lat1,lon1,lat2,lon2,zoom,1));
		 long finalTime = System.currentTimeMillis();
		 LOGGER.info("Time for request: " + (finalTime-initTime));
		 return returnVal;

	}
	
	@RequestMapping(value = "/app/get/"+WILDSTREAMS, method = RequestMethod.GET)
	public ResponseEntity<Void> getWildernessStreams() {
		LOGGER.info("Get Wilderness Streams");
		return ResponseEntity.status(HttpStatus.OK).build();

	}
	
	@RequestMapping(value = "/app/get/"+WILDSTREAMS+"/name/{name}", method = RequestMethod.GET)
	public @ResponseBody String getWildernessStreams(@PathVariable String name) {
		LOGGER.info("Get Wilderness Streams");
		return streamService.buildGeoJsonFromIterableWater(waterRepo.findStreamAndCoorsByName(name));

	}
	
	@RequestMapping(value = "/app/get/"+WILDSTREAMS+"/zoom/{zoom}/loc/{lat1}/{lon1}/{lat2}/{lon2}", method = RequestMethod.GET)
	public @ResponseBody String getWildernessStreams(@PathVariable BigDecimal lat1,@PathVariable BigDecimal lon1,@PathVariable BigDecimal lat2
			,@PathVariable BigDecimal lon2,@PathVariable Double zoom) {
		LOGGER.info("Get Wilderness Streams");
		return streamService.buildGeoJsonFromIterableWater(waterRepo.findStreamsByLocAndType(lat1,lon1,lat2,lon2,zoom,0));

	}
		
	@RequestMapping(value = "/app/get/"+STOCKEDSTREAMS, method = RequestMethod.GET)
	public  ResponseEntity<Void> getStockedStreams() {
		LOGGER.info("Get Stocked Streams");
		return ResponseEntity.status(HttpStatus.OK).build();

	}
	
	@RequestMapping(value = "/app/get/"+STOCKEDSTREAMS+"/name/{name}", method = RequestMethod.GET)
	public  @ResponseBody String getStockedStreams(@PathVariable String name) {
		LOGGER.info("Get Stocked Streams");
		return streamService.buildGeoJsonFromIterableWater(waterRepo.findStreamAndCoorsByName(name));

	}
	
	@RequestMapping(value = "/app/get/"+STOCKEDSTREAMS+"/zoom/{zoom}/loc/{lat1}/{lon1}/{lat2}/{lon2}", method = RequestMethod.GET)
	public  @ResponseBody String getStockedStreams(@PathVariable BigDecimal lat1,@PathVariable BigDecimal lon1,@PathVariable BigDecimal lat2
			,@PathVariable BigDecimal lon2,@PathVariable Double zoom) {
		LOGGER.info("Get Stocked Streams");
		return streamService.buildGeoJsonFromIterableWater(waterRepo.findStreamsByLocAndType(lat1,lon1,lat2,lon2,zoom,6));

	}
	
	@RequestMapping(value = "/app/get/"+STOCKEDWATERS, method = RequestMethod.GET)
	public ResponseEntity<Void> getStockedLakes() {
		LOGGER.info("Get Stocked Lakes");
		return ResponseEntity.status(HttpStatus.OK).build();

	}
	
	@RequestMapping(value = "/app/get/"+STOCKEDWATERS+"/name/{name}", method = RequestMethod.GET)
	public @ResponseBody String getStockedLakes(@PathVariable String name) {
		LOGGER.info("Get Stocked Lakes");
		return streamService.buildGeoJsonFromIterableWater(waterRepo.findStreamAndCoorsByName(name));

	}
	
	@RequestMapping(value = "/app/get/"+STOCKEDWATERS+"/zoom/{zoom}/loc/{lat1}/{lon1}/{lat2}/{lon2}", method = RequestMethod.GET)
	public @ResponseBody String getStockedLakes(@PathVariable BigDecimal lat1,@PathVariable BigDecimal lon1,@PathVariable BigDecimal lat2,
			@PathVariable BigDecimal lon2,@PathVariable Double zoom) {
		LOGGER.info("Get Stocked Lakes");
		return streamService.buildGeoJsonFromIterableWater(waterRepo.findStreamsByLocAndType(lat1,lon1,lat2,lon2,zoom,5));

	}
	
	@RequestMapping(value = "/app/get/"+NATREPROSTREAMS, method = RequestMethod.GET)
	public ResponseEntity<Void> getNaturalStreams() {
		LOGGER.info("Get Natural Streams");
		return ResponseEntity.status(HttpStatus.OK).build();

	}

	@RequestMapping(value = "/app/get/"+NATREPROSTREAMS+"/name/{name}", method = RequestMethod.GET)
	public @ResponseBody String getNaturalStreams(@PathVariable String name) {
		LOGGER.info("Get Natural Streams");
		return streamService.buildGeoJsonFromIterableWater(waterRepo.findStreamAndCoorsByName(name));

	}
	
	@RequestMapping(value = "/app/get/"+NATREPROSTREAMS+"/zoom/{zoom}/loc/{lat1}/{lon1}/{lat2}/{lon2}", method = RequestMethod.GET)
	public @ResponseBody String getNaturalStreams(@PathVariable BigDecimal lat1,@PathVariable BigDecimal lon1,@PathVariable BigDecimal lat2
			,@PathVariable BigDecimal lon2,@PathVariable Double zoom) {
		LOGGER.info("Get Natural Streams");
		String retString = streamService.buildGeoJsonFromIterableWater(waterRepo.findStreamsByLocAndType(lat1,lon1,lat2,lon2,zoom,2));
		//LOGGER.info(retString);
		return retString;

	}
	
	
	@RequestMapping(value = "/app/get/"+SPECIALREGSTREAMS, method = RequestMethod.GET)
	public ResponseEntity<Void> getSpecialRegStreams() {
		LOGGER.info("Get Natural Streams");
		return ResponseEntity.status(HttpStatus.OK).build();

	}

	@RequestMapping(value = "/app/get/"+SPECIALREGSTREAMS+"/name/{name}", method = RequestMethod.GET)
	public @ResponseBody String getSpecialRegStreams(@PathVariable String name) {
		LOGGER.info("Get Natural Streams");
		return streamService.buildGeoJsonFromIterableWater(waterRepo.findStreamAndCoorsByName(name));

	}
	
	@RequestMapping(value = "/app/get/"+SPECIALREGSTREAMS+"/zoom/{zoom}/loc/{lat1}/{lon1}/{lat2}/{lon2}", method = RequestMethod.GET)
	public @ResponseBody String getSpecialRegStreams(@PathVariable BigDecimal lat1,@PathVariable BigDecimal lon1,@PathVariable BigDecimal lat2
			,@PathVariable BigDecimal lon2,@PathVariable Double zoom) {
		LOGGER.info("Get Natural Streams");
		return streamService.buildGeoJsonFromIterableWater(waterRepo.findStreamsByLocAndType(lat1,lon1,lat2,lon2,zoom,3));

	}
	
	@RequestMapping(value = "/app/get/"+SPECIALREGLAKES, method = RequestMethod.GET)
	public ResponseEntity<Void> getSpecialRegLakes() {
		LOGGER.info("Get Natural Streams");
		return ResponseEntity.status(HttpStatus.OK).build();

	}

	@RequestMapping(value = "/app/get/"+SPECIALREGLAKES+"/name/{name}", method = RequestMethod.GET)
	public @ResponseBody String getSpecialRegLakes(@PathVariable String name) {
		LOGGER.info("Get Natural Streams");
		return streamService.buildGeoJsonFromIterableWater(waterRepo.findStreamAndCoorsByName(name));

	}
	
	@RequestMapping(value = "/app/get/"+SPECIALREGLAKES+"/zoom/{zoom}/loc/{lat1}/{lon1}/{lat2}/{lon2}", method = RequestMethod.GET)
	public @ResponseBody String getSpecialRegLakes(@PathVariable BigDecimal lat1,@PathVariable BigDecimal lon1,@PathVariable BigDecimal lat2
			,@PathVariable BigDecimal lon2,@PathVariable Double zoom) {
		LOGGER.info("Get Natural Streams");
		return streamService.buildGeoJsonFromIterableWater(waterRepo.findStreamsByLocAndType(lat1,lon1,lat2,lon2,zoom,4));

	}
	
	@RequestMapping(value = "/app/getShops", method = RequestMethod.GET)
	public ResponseEntity<Void> getShops() {
		LOGGER.info("Get Shops");
		return ResponseEntity.status(HttpStatus.OK).build();

	}

	//TODO: Put search in another controller
	@RequestMapping(value = "/app/Search", method = RequestMethod.POST)
	public ResponseEntity<Void> search() {
		LOGGER.info("Search");
		return ResponseEntity.status(HttpStatus.OK).build();

	}



		



}
