package com.pastreams.services.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.pastreams.services.streamsdb2.Water;

public interface WaterRepo2 extends CrudRepository<Water,Long>{
	
	Iterable<Water> findAll();
	
	@Query("select w from Water w where w.name = ?1")
	Water findStreamByName(String name);
	
	@Query("select w from Water w where w.name = ?1")
	Iterable<Water> findStreamAndCoorsByName(String name);
	
	@Query("select w from Water w where w.watertype = ?1 and w.name = ?2")
	Iterable<Water> findStreamByTypeAndName(Integer type,String name);
	
	//w.watertype.watertypeid = ?5 and 
	@Query("select w from Water w where w.watertype.watertypeid = ?6 and w.zoomlevel < ?5 and  w.latitude > ?1 and w.longitude > ?2 and w.latitude < ?3 and w.longitude < ?4")
	Iterable<Water> findStreamsByLocAndType(BigDecimal lat1, BigDecimal lon1,BigDecimal lat2, BigDecimal lon2, Double zoom, Integer type);

}
